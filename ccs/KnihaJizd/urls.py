from django.urls import path
from . import views

urlpatterns = [
    path('', views.hello),
    path("kontakt", views.contact),
    path("prihlaseni", views.login)
]