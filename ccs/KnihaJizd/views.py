from django.shortcuts import render
from django.http import HttpResponse

def hello(request):
    return render(request, "knihajizd/home.html")

def contact(request):
    return HttpResponse("Kontakt")

def login(request):
    return HttpResponse("přihlášení")